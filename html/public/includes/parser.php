<?php

    function chapterParser($string) {
        ob_start();
        $array = explode(';',$string);

        foreach($array as $key => $array_) {
            $array[$key] = explode('(',$array_);
        }

        foreach($array as $key => $array_) {
            trim($array_[1], "()");
            $time   = explode(":", $array_[1]);
            $minute = $time[0] * 60 * 1000;
            $sec    = $time[1] * 1000;
            $array[$key][1] = $minute + $sec;
        }

        echo '
                {
                    "indexes": [';
                        foreach($array as $key => $array_) {
                            echo'
                                {
                                    "description": "'.$array_[0].'",
                                    "time": "'.$array_[1].'",
                                    "title": "'.$array_[0].'"
                                }';
                            if( next( $array ) ) {
                                echo ',';
                            }
                        }
        echo '
                    ]
                }'
        ;
        return ob_get_clean();
    }
?>



