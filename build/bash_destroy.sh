#!/bin/bash
docker stop docker_compose_docker_db_aubu_1
docker stop docker_compose_docker_web_aubu_1
docker rm docker_compose_docker_db_aubu_1
docker rm docker_compose_docker_web_aubu_1
docker rmi docker_compose_docker_db_aubu
docker rmi docker_compose_docker_web_aubu
docker rmi mysql:5.7
docker rmi php:7.0-apache
rm -Rf ./docker_compose/mysql/*
